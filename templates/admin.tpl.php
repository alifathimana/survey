<html>
<head><title>Admin</title></head> 
<body>
<h3>Questions</h3>
<br><br><br>
<div>
<table>
<thead>
<th>SL No.</th><th>Question</th><th colspan = '2'>Action</th>
</thead>
<?php
foreach ($questions as $quest) {
  $id = $quest['id'];
  echo "<tr><td>{$quest['id']}</td><td>{$quest['questions']}</td><td><a href = '/index.php/editquestion?link=$id'>Edit question</a></td><td><a href = '/index.php/deletequestion?link=$id'>Delete question</a></td></tr>";
}
?>
</table>
<nav>
<ul>
<br><br><li><a href = '/index.php/addquestion'>Add question</a></li><br>
<br><br><li><a href = '/index.php/viewscores'>View scores</a></li><br><br><br>
<li><a href = '/index.php/logout'>Logout</a></li>
</ul>
</nav>
</div>
</body>
<?php 

require_once 'templates/layout.tpl.php';
?>
</html>
