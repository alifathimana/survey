<html>
<style>
h3 {
  color:green;
  background:grey;
  text-align:center;
  font-size:30pt;
} 
body {
  margin:20px 50px 50px 20px;
  padding:20px 10px 10px 20px;
  background:grey;
}
table {
  border:1px dashed black;
}
td {
  border:1px dashed black;
  padding:5px 5px 5px 5px;
}
th {
  border:1px dashed black;
  padding:5px 5px 5px 5px;
}
div {
  margin:10px 10px 10px 10px;
  padding:10px 10px 10px 10px;
  color:green;
}
span {
  margin:50px 50px 50px 50px;
  padding:20px 20px 20px 20px;
  background:grey;
}
a {
  text-decoration : none;
  font-weight:bold;
  color:blue;
}
a:hover {
  text-decoration : underline;
  font-weight:bold;
  color:green;
}
ul{
  list-style-type:none;
  margin:0;
  padding:0;
}
li{
  display:inline;
}
</style> 
</html>