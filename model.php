<?php

session_start();

/**
 * Function to establish a connection to the database.
 */
function open_database_connection() {
  $servername = "localhost";
  $username = "root";
  $password = "root";
  $dbname = "survey";
  $link = new PDO("mysql:servername=$servername;dbname=$dbname", $username, $password);
  $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $link;
}

/**
 * Function to check login action.
 */
function login_check($link) {
  $user = $_POST['username'];
  $pass = $_POST['password'];
  $stmt = $link->prepare("SELECT * FROM userlogin WHERE username = :username AND password = :password");
  $stmt->bindParam(':username', $user);
  $stmt->bindParam(':password', $pass);
  $stmt->execute();
  $person = $stmt->fetchAll();
  if ($person) {
    $_SESSION['username'] = $user;
    $_SESSION['password'] = $pass;
    header("Location: http://survey/index.php/signedin");
  }
  else {
    require_once 'login.php';
  }
}

/**
 * Function for admin.
 */
function admin_qn($link) {
  if (($_SESSION['username']) && ($_SESSION['password'])) {
    $stmt = $link->prepare("SELECT * FROM questions WHERE flag = 0");
    $stmt->execute();
    $questions = $stmt->fetchAll();
    foreach ($questions as $ques) {
      $qid = $ques['id'];
    }
    $stmt = $link->prepare("SELECT * FROM options WHERE qid = :qid AND flag = 0 ");
    $stmt->bindParam(':qid', $qid);
    $stmt->execute();
    $options = $stmt->fetchAll();
    require_once 'templates/admin.tpl.php';
  }
  else {
    header("Location: http://survey/");
  }
}

/**
 * Function to add question.
 */
function add_question($link) {
  require_once 'templates/addquestion.tpl.php';
}

/**
 * Function to add question to database.
 */
function add_qn($link) {
  $ques = $_POST['question'];
  $flag = 0;
  $stmt = $link->prepare("INSERT INTO questions (questions, flag) VALUES (:ques, :flag)");
  $stmt->bindParam(':ques', $ques);
  $stmt->bindParam(':flag', $flag);
  $stmt->execute();
  $stmt = $link->prepare("SELECT id FROM questions WHERE questions = :ques");
  $stmt->bindParam(':ques', $ques);
  $stmt->execute();
  $qid = $stmt->fetchAll();
  foreach ($qid as $id) {
    $quesid = $id['id'];
  }
  
  $stmt = $link->prepare("INSERT INTO options (qid, options, flag) VALUES (:qid, :options, :flag)");
  $stmt->bindParam(':qid', $quesid);
  $stmt->bindParam(':flag', $flag);
  foreach ($_POST['option'] as $opt) {
    $stmt->bindParam(':options', $opt);
    $stmt->execute();
  }
  header("Location: http://survey/index.php/signedin");
}

/**
 * Function to edit interview.
 */
function edit_question($id, $link) {
  $stmt = $link->prepare("SELECT * FROM questions WHERE id = :id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();
  $questions = $stmt->fetchAll();
  $stmt = $link->prepare("SELECT * FROM options WHERE qid = :id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();
  $options = $stmt->fetchAll();
  require_once 'templates/editquestion.tpl.php';
}

/**
 * Function to update question in database.
 */
function edit_qn($link) {
  $qn = $_POST['question'];
  $qid = $_POST['qid'];
  $stmt = $link->prepare("UPDATE questions SET questions = :question WHERE id = :qid");
  $stmt->bindParam(':qid', $qid);
  $stmt->bindParam(':question', $qn);
  $stmt->execute();
  /** Write function to edit option values also. */
}

/**
 * Function to delete question.
 */
function delete_question($qid, $link) {
  $stmt = $link->prepare("UPDATE options SET flag = 1 WHERE qid = :id");
  $stmt->bindParam(':id', $qid);
  $stmt->execute();
  $stmt = $link->prepare("UPDATE questions SET flag = 1 WHERE id = :id");
  $stmt->bindParam(':id', $qid);
  $stmt->execute();
}

/**
 * Function for a participant to attend the quiz.
 */
function participant_qn($link) {
  $stmt = $link->prepare("SELECT * from questions");
  $stmt->execute();
  $questions = $stmt->execute();
  require_once 'quiz.tpl.php';
}

/**
 * Function to create a quiz.
 */
function create_qz($link) {
  $stmt = $link->prepare("SELECT COUNT(questions) as ct FROM questions");
  $stmt->execute();
  $ct = $stmt->fetchAll();
  $c = $ct[0][0];
  for ($i = 1; $i <= $c; $i++) {
    $stmt = $link->prepare("SELECT * FROM questions where id = :id and flag = 0");
    $stmt->bindParam(':id', $i);
    $stmt->execute();
    $questions[$i] = $stmt->fetchAll();
    $stmt = $link->prepare("SELECT * FROM options WHERE qid = :qid and flag = 0");
    $stmt->bindParam(':qid', $i);
    $stmt->execute();
    $options[$i] = $stmt->fetchAll();
  }
  $k = $i - 1;
  require_once 'templates/createquiz.tpl.php';
}

/**
 * Function to update the quiz.
 */
function create_quiz($link) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $string = $_POST['string'];
  $stmt = $link->prepare("INSERT INTO creator (name, email, stringgenerated) values (:name, :email, :string)");
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':string', $string);
  $stmt->execute();
  $stmt = $link->prepare("SELECT id from creator where name = :name and stringgenerated = :string");
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':string', $string);
  $stmt->execute();
  $id = $stmt->fetchAll();
  $i_d = $id[0][0];
  $k = $_POST['count'];
  foreach ($_POST['answer'] as $quesid => $opid) {
    $qid = $quesid;
    $cid = $i_d;
    $optionid = $opid;
    $stmt = $link->prepare("INSERT INTO rightanswers (qid, cid, optionid) values (:qid, :cid, :optionid)");
    $stmt->bindParam(':qid', $qid);
    $stmt->bindParam(':cid', $cid);
    $stmt->bindParam(':optionid', $optionid);
    $stmt->execute();
  }
  echo "Share this URL with your friends!!!!!!!<br><br>";
  echo "http://survey/index.php/answer?string=$string";
  echo "<br><br><a href = '/index.php/creator'>Back</a>";
}

/**
 * Function to attend the quiz.
 */
function answer_quiz($string, $link) {
  $stmt = $link->prepare("SELECT COUNT(questions) as ct FROM questions");
  $stmt->execute();
  $ct = $stmt->fetchAll();
  $c = $ct[0][0];
  for ($i = 1; $i <= $c; $i++) {
    $stmt = $link->prepare("SELECT * FROM questions where id = :id and flag = 0");
    $stmt->bindParam(':id', $i);
    $stmt->execute();
    $questions[$i] = $stmt->fetchAll();
    $stmt = $link->prepare("SELECT * FROM options WHERE qid = :qid and flag = 0");
    $stmt->bindParam(':qid', $i);
    $stmt->execute();
    $options[$i] = $stmt->fetchAll();
  }
  $k = $i - 1;
  require_once 'templates/quiz.tpl.php';
}

/**
 * Function to attend quiz.
 */
function quiz_a($link) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $string = $_POST['string'];
  $stmt = $link->prepare("SELECT id FROM creator where stringgenerated = :string");
  $stmt->bindParam(':string', $string);
  $stmt->execute();
  $id = $stmt->fetchAll();
  $i_d = $id[0][0];
  $stmt = $link->prepare('INSERT INTO user (name, email, creatorid) VALUES (:name, :email, :creatorid)');
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':creatorid', $i_d);
  $stmt->execute();
  $stmt = $link->prepare("SELECT max(id) as id FROM user");
  $stmt->execute();
  $uid = $stmt->fetchAll();
  $u_id = $uid[0][0];
  foreach ($_POST['answer'] as $quesid => $opid) {
    $qid = $quesid;
    $optionid = $opid;
    $stmt = $link->prepare("INSERT INTO useranswers (userid, qid, optionid) values (:userid, :qid, :optionid)");
    $stmt->bindParam(':qid', $qid);
    $stmt->bindParam(':userid', $u_id);
    $stmt->bindParam(':optionid', $optionid);
    $stmt->execute();
  }
  header("Location: http://survey/index.php/mark?link=$u_id&cr=$i_d");
}

/**
 * Function to find the score.
 */
function mark_a($u_id, $i_d, $link) {
  $stmt = $link->prepare("SELECT COUNT(*) AS mark FROM rightanswers a join useranswers b on a.optionid = b.optionid where a.cid = :cid and b.userid = :uid ");
  $stmt->bindParam(':cid', $i_d);
  $stmt->bindParam(':uid', $u_id);
  $stmt->execute();
  $marks = $stmt->fetchAll();
  $mark = $marks[0][0];
  $stmt = $link->prepare("INSERT INTO score (score, cid, uid) VALUES (:score, :cid, :uid)");
  $stmt->bindParam(':score', $mark);
  $stmt->bindParam(':cid', $i_d);
  $stmt->bindParam(':uid', $u_id);
  $stmt->execute();
  echo "<br><br>Your mark is " . $mark;
}

/**
 * Function to view marks of all friends of a particular creator.
 */
function view_marks($link) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $stmt = $link->prepare("SELECT id FROM creator WHERE name = :name AND email = :email");
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':email', $email);
  $stmt->execute();
  $cr = $stmt->fetchAll();
  $id = $cr[0][0];
  $stmt = $link->prepare("SELECT score, name, email from score a join user b on a.uid = b.id");
  $stmt->execute();
  $list = $stmt->fetchAll();
  require_once 'templates/list.tpl.php';
}

/**
 * Function to view scores.
 */
function view_scores($link) {
  $stmt = $link->prepare("SELECT name, email, score FROM score a JOIN user b ON a.uid = b.id");
  $stmt->execute();
  $res = $stmt->fetchAll();
  require_once 'templates/viewscores.tpl.php';
}

/**
 * Function to close database connection.
 */
function close_database_connection($link) {
  $link = NULL;
}
?>