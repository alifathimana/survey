<?php

require_once 'model.php';
/**
 * Function to check login action.
 */
function logincheck() {
  require_once 'model.php';
  $link = open_database_connection();
  login_check($link);
  close_database_connection($link);
}

/**
 * Function to add new questions.
 */
function admin() {
  
  $link = open_database_connection();
  admin_qn($link);
  close_database_connection($link);
}

/**
 * Function to add question.
 */
function addquestion() {
  require_once 'model.php';
  $link = open_database_connection();
  add_question($link);
  close_database_connection($link);
}

/**
 * Function to add question to database.
 */
function addqn() {
  require_once 'model.php';
  $link = open_database_connection();
  add_qn($link);
  close_database_connection($link);
}

/**
 * Function to edit question.
 */
function editquestion($id) {
  require_once 'model.php';
  $link = open_database_connection();
  edit_question($id, $link);
  close_database_connection($link);
}

/**
 * Function to update question in database.
 */
function editqn() {
  require_once 'model.php';
  $link = open_database_connection();
  edit_qn($link);
  close_database_connection($link);
}

/**
 * Function to delete question in a database.
 */
function deletequestion($qid) {
  require_once 'model.php';
  $link = open_database_connection();
  delete_question($qid, $link);
  close_database_connection($link);
}

/**
 * Function for a participant to attend quiz.
 */
function participant() {
  require_once 'model.php';
  $link = open_database_connection();
  participant_qn($link);
  close_database_connection($link);
}

/**
 * Function to create a quiz.
 */
function create() {
  //require 'model.php';
  $link = open_database_connection();
  create_qz($link);
  close_database_connection($link);
}

/**
 * Function to create quiz.
 */
function createquiz() {
  require_once 'model.php';
  $link = open_database_connection();
  create_quiz($link);
  close_database_connection($link);
}

/**
 * Function to answer quiz.
 */
function answer($string) {
  require_once 'model.php';
  $link = open_database_connection();
  answer_quiz($string, $link);
  close_database_connection($link);
}

/**
 * Function to update the entered values to database.
 */
function quiz() {
  require_once 'model.php';
  $link = open_database_connection();
  quiz_a($link);
  close_database_connection($link);
}

/**
 * Function to show marks.
 */
function marks($u_id, $i_d) {
  require_once 'model.php';
  $link = open_database_connection();
  mark_a($u_id, $i_d, $link);
  close_database_connection($link);
}

/**
 * Function to view marks of all friends of a creator.
 */
function viewmarks() {
  require_once 'model.php';
  $link = open_database_connection();
  view_marks($link);
  close_database_connection($link);
}

/**
 * Logout.
 */
function logoutaction() {
  //session_unset();
  session_destroy();
  header("Location: http://survey/");
}

/**
 * Function to view scores of all participants.
 */
function viewscores() {
  $link = open_database_connection();
  view_scores($link);
  close_database_connection($link);
}
?>