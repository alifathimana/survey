<?php

require_once 'controllers.php';
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if ('/' == $uri) {
  require_once 'login.php';
}
elseif ('/index.php/logincheck' == $uri) {
  logincheck();
}
elseif ('/index.php/signedin' == $uri) {
  admin();
}
elseif ('/index.php/addquestion' == $uri) {
  addquestion();
}
elseif ('/index.php/addqn' == $uri) {
  addqn();
}
elseif ('/index.php/logout' == $uri) {
  logoutaction();
}
elseif (('/index.php/editquestion' == $uri) && isset($_GET['link'])) {
  $id = $_GET['link'];
  editquestion($id);
}
elseif ('/index.php/editqn' == $uri) {
  editqn();
}
elseif (('/index.php/createquiz' == $uri)) {
  createquiz();
}
elseif (('/index.php/deletequestion' == $uri) && isset($_GET['link'])) {
  $qid = $_GET['link'];
  deletequestion($qid);
}
elseif (('/index.php/participant' == $uri)) {
  participant();
}
elseif (('/index.php/creator' == $uri)) {
  create();
}
elseif (('/index.php/answer' == $uri) && isset($_GET['string'])) {
  $string = $_GET['string'];
  answer($string);
}
elseif (('/index.php/quiz' == $uri)) {
  quiz();
}
elseif (('/index.php/mark' == $uri) && isset($_GET['link']) && isset($_GET['cr'])) {
  $u_id = $_GET['link'];
  $i_d = $_GET['cr'];
  marks($u_id, $i_d);
}
elseif (('/index.php/viewmarks' == $uri)) {
  viewmarks();
}
elseif ('/index.php/listmarks' == $uri) {
  require_once 'templates/detailspage.tpl.php';
}
elseif ('/index.php/viewscores' == $uri) {
  viewscores();
}
else {
  echo $uri;
  header('http/1.1 404 Not Found');
  echo "<h1>page not found</h1>";
}
?>